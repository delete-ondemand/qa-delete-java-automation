package com.fuse8.qa.NDM;

import org.openqa.selenium.By;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.List;

public class ErrorPage extends BaseClass {
    List<String> dirs = Arrays.asList("assets", "cached", "css", "fonts", "images", "js", "media", "packages", "storage", "svg", "thumbs", "vendor");


    @Test(enabled = true, dataProvider = "urls")
    public void errorPage(String url) throws Exception {

        for (int i = 0; i < dirs.size(); i++) {
            driver.get(url+ "/" + dirs.get(i));
            driver.findElement(By.xpath("//h1[contains(text(),'Whoops')]"));
        }
    }
}
