package com.fuse8.qa.NDM;

import com.fuse8.qa.TestNGHelper;
import org.testng.annotations.DataProvider;

public class BaseClass extends TestNGHelper {

    String login = "zhanna.topchiy+ndm@deleteagency.com";
    String password = "1qaz2wsX1";

    //Browser authentication
    String name = "joinnetwork";
    String pass = "2-Partly-Surely-&-8-%-6";

    @DataProvider(name = "staging")
    public Object[][] StagingDPMethod() {
        return new Object[][]{
                {"https://" + name + ":" + pass + "@staging.healthservicediscounts.com"},
                {"https://" + name + ":" + pass + "@staging.discountsforcarers.com"},
                {"https://" + name + ":" + pass + "@staging.discountsforteachers.co.uk"},
                {"https://" + name + ":" + pass + "@staging.charityworkerdiscounts.com"}
        };
    }

    @DataProvider(name = "live")
    public Object[][] LiveDPMethod() {
        return new Object[][]{
                {"https://www.healthservicediscounts.com"},
                {"https://www.discountsforcarers.com"},
                {"https://www.discountsforteachers.co.uk"},
                {"https://www.charityworkerdiscounts.com"}};
    }

    static {
        testName = "NDM Test";
        hasNetwork = false;
        hasVisual = false;
        hasVideo = true;
        hasConsole = false;
    }

}
