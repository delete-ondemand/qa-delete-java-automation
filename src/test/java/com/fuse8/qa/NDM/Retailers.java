package com.fuse8.qa.NDM;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Retailers extends BaseClass {

    @Test(enabled = true, dataProvider = "urls")
    public void testRetailerOrder(String url) throws IOException { //check if all thr retailers in alphabetic order

        driver.get (url + "/retailers-a-z");

        List<String> retailers = driver.findElements(By.xpath("//article[@class='content']//div[@class='columns']//a")).stream()
                //.filter(el -> el.getAttribute("href") != null)
                .map(el -> el.getText())
                .distinct()
                .collect(Collectors.toList());

        List<String> azRetailers = new ArrayList<>(retailers);

        Collections.sort(azRetailers, String.CASE_INSENSITIVE_ORDER);

        Assert.assertTrue(retailers.equals(azRetailers), "Retailers are not sorted alphabetically!");


    }
}

