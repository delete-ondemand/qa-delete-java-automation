package com.fuse8.qa.NDM;

import com.fuse8.qa.NDM.pageObject.SignIn;
import com.github.romankh3.image.comparison.ImageComparison;
import com.github.romankh3.image.comparison.ImageComparisonUtil;
import com.github.romankh3.image.comparison.model.ImageComparisonResult;
import com.github.romankh3.image.comparison.model.ImageComparisonState;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class GetOffer extends BaseClass {

    @Test(enabled = true, dataProvider = "live")
    public void screenShot(String url) throws IOException {
        driver.get(url);
        new SignIn(driver).loginAndCloseCookies(login, password);

        //WebElement search = driver.findElement(By.xpath("//input[@class='button hero-search-button']"));
        WebElement search = driver.findElement(By.xpath("//a[@class='header__logo']"));
        // capture screenshot with getScreenshotAs() of the dropdown
        File f = search.getScreenshotAs(OutputType.FILE);
        String siteName = url.replace("https://www.", "") + "-search-button.png";
        FileUtils.copyFile(f, new File(siteName));

        //load images to be compared:
        BufferedImage expectedImage = ImageComparisonUtil.readImageFromResources("Logo.png"); //design logo
        BufferedImage actualImage = ImageComparisonUtil.readImageFromResources(siteName);

        // where to save the result (leave null if you want to see the result in the UI)
        File resultDestination = new File("result" + siteName);

        //Create ImageComparison object and compare the images.
        ImageComparisonResult imageComparisonResult = new ImageComparison(expectedImage, actualImage, resultDestination).compareImages();

        //Check the result
        Assert.assertEquals(ImageComparisonState.MATCH, imageComparisonResult.getImageComparisonState());

        //Assert.assertTrue(new File(siteName).exists());
    }

    @Test(enabled = true, dataProvider = "live")
    public void myAccount(String url) throws IOException {
        driver.get(url);
        new SignIn(driver).loginAndCloseCookies(login, password);
        driver.findElement(By.xpath("//div[@class='header__usercontrols']//a[contains(@href,'my-profile')]")).click();
        driver.findElement(By.xpath("//input[@id = 'first_name']")).sendKeys("ZhTest");
        driver.findElement(By.xpath("//input[@type = 'password']")).sendKeys(password);
        driver.findElement(By.xpath("//input[@type = 'submit']")).submit();


    }

    @Test(enabled = true, dataProvider = "live")
    public void testGetOfferWhtCashback(String url) { // pick up all links from site and check if "Get code" has the second word in uppercase (Get Code - wrong variant)

        driver.manage().timeouts().implicitlyWait(Duration.ofMillis(500L));

        driver.get(url);
        new SignIn(driver).loginAndCloseCookies(login, password);

        Map<String, Boolean> allLinksMap = new HashMap<>();
        //allLinksMap.put(url, false);
        int errorCount = 0;

        //Find all links on open page from main navigation
        List<String> allNavLinks = collectLinks("//nav[@class='main-navigation nav--loaded']//li[@class='item-menu']/a[@class='toggleMega']");

        allNavLinks.forEach(link -> {
            driver.get(link);
            //collect all offers
            List<String> allOfferLinks = collectLinks("//a[contains(@class,'offer__link')]");
            //add to map
            for (String el : allOfferLinks) {
                allLinksMap.putIfAbsent(el, false);
            }
            //collect pages
            List<String> allOfferPages = collectLinks("//a[contains(@class,'page-link')]");
            allOfferPages.forEach(page -> {
                driver.get(page);
                List<String> offerLinks = collectLinks("//a[contains(@class,'offer__link')]");
                //add to map
                for (String el : offerLinks) {
                    allLinksMap.putIfAbsent(el, false);
                }
            });
        });
        System.out.println("Check all offers");
        int i = 0;

        System.out.println("Total offers = " + allLinksMap.size());

        List<String> cashBackOffers = new ArrayList<>();
        while (true) {
            //Get non-visited link or break loop if all links visited (all links are visited = all values in the map are true)
            String currentLink = getKey(allLinksMap, false);
            if (currentLink == null) {
                break; // break loop condition - all links are visited (all values are true). See getKey method
            }
            //System.out.println(currentLink);

            i++;
            if ((i * 100 / allLinksMap.size()) % 20 == 0) {
                System.out.println("... " + (i * 100 / allLinksMap.size()) + "%");
            }

            //Open non-visited link from map
            driver.get(currentLink);
//
//            if (currentLink.contains("sainsburys-3-cashback-4028"))
//                System.out.println();

            //Mark it as visited
            allLinksMap.put(currentLink, true); //ссылка заменит саму себя, тк ключи не могут совпадать

            try {
                driver.findElement(By.xpath("//div[@class='offer-button-wrap']//a[contains(text(), 'Card')]"));
                System.out.println("Cashback on page " + currentLink);
                cashBackOffers.add(currentLink);

            } catch (NoSuchElementException e) {

            }


        }
//Save results to csv
        String objectsCommaSeparated = cashBackOffers.stream().collect(Collectors.joining(",\n"));
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy.MM.dd-HH.mm.ss");
        LocalDateTime now = LocalDateTime.now();
        //TODO: use regex to site name
        String siteName = null;
        if (url.contains("healthservicediscounts"))
            siteName = "HSD";
        if (url.contains("discountsforcarers"))
            siteName = "DFC";
        if (url.contains("discountsforteachers"))
            siteName = "DFT";
        if (url.contains("charityworkerdiscounts"))
            siteName = "CWD";
        //create csv file "data-scheme-result"
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(dtf.format(now) + " " + siteName + " result.csv"))) {
            writer.write(objectsCommaSeparated);
            System.out.println("");
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Result file was not created");
        }

        //Final result messages
        System.out.println("Tests done. " + allLinksMap.size() + " pages were verified.");
        if (errorCount > 0) {
            Assert.fail("Test failed! See errors above ↑. Count of errors = " + errorCount);
        }
    }

    @Test(enabled = true, dataProvider = "urls")
    public void testGetOffer(String url) throws IOException { // pick up all links from site and check if "Get code" has the second word in uppercase (Get Code - wrong variant)

        driver.manage().timeouts().implicitlyWait(500, TimeUnit.MILLISECONDS);

        Map<String, Boolean> allLinksMap = new HashMap<>();
        //allLinksMap.put(url, false);

        int errorCount = 0;

        //login to the site to get offer
        driver.get(url + "/login");
        driver.findElement(By.xpath("//input[@id='email']")).sendKeys(login);
        driver.findElement(By.xpath("//input[@id = 'password']")).sendKeys(password);
        driver.findElement(By.xpath("//input[@type = 'submit']")).submit();

        //Find all links on open page from main navigation
        List<String> allNavLinks = collectLinks("//nav[@class='main-navigation nav--loaded']//li[@class='item-menu']/a[@class='toggleMega']");

        allNavLinks.forEach(link -> {
            driver.get(link);
            //collect all offers
            List<String> allOfferLinks = collectLinks("//a[contains(@class,'offer__link')]");
            //add to map
            for (String el : allOfferLinks) {
                allLinksMap.putIfAbsent(el, false);
            }
            //collect pages
            List<String> allOfferPages = collectLinks("//a[contains(@class,'page-link')]");
            allOfferPages.forEach(page -> {
                driver.get(page);
                List<String> offerLinks = collectLinks("//a[contains(@class,'offer__link')]");
                //add to map
                for (String el : offerLinks) {
                    allLinksMap.putIfAbsent(el, false);
                }
            });
        });
        System.out.println("Check all offers");
        while (true) {
            //Get non-visited link or break loop if all links visited (all links are visited = all values in the map are true)
            String currentLink = getKey(allLinksMap, false);
            if (currentLink == null) {
                break; // break loop condition - all links are visited (all values are true). See getKey method
            }
            System.out.println(currentLink);

            //Open non-visited link from map
            driver.get(currentLink);

            //Mark it as visited
            allLinksMap.put(currentLink, true); //ссылка заменит саму себя, тк ключи не могут совпадать

            try {
                driver.findElement(By.xpath("//a[contains(@data-objectid,'Code')]"));
                System.out.println("Attention!!! Code is Uppercase! On page " + currentLink);
                errorCount++;
            } catch (NoSuchElementException e) {

            }

            //Add all new links that are missed in the map
            for (String pageLink : allNavLinks) {
                //Check if link exists (not 404)
                if (!allLinksMap.containsKey(pageLink)) {
                    URL testUrl = new URL(pageLink);
                    HttpURLConnection huc = (HttpURLConnection) testUrl.openConnection();
                    int responseCode = huc.getResponseCode();
                    if (responseCode == HttpURLConnection.HTTP_NOT_FOUND) {
                        System.out.println("Attention! Page " + pageLink + " is not found! Link is placed on " + currentLink);
                    }
                    huc.disconnect();
                }
            }
        }

        //Final result messages
        System.out.println("Tests done. " + allLinksMap.size() + " pages were verified.");
        if (errorCount > 0) {
            Assert.fail("Test failed! See errors above ↑. Count of errors = " + errorCount);
        }
    }

    private List<String> collectLinks(String xpath) {
        return driver.findElements(By.xpath(xpath)).stream()
                .filter(el -> el.getAttribute("href") != null && !el.getAttribute("href").startsWith("tel:"))
                .map(el -> el.getAttribute("href").replaceAll("[\\/#]*$", ""))
                .distinct()
                .collect(Collectors.toList());
    }

    public String getKey(Map<String, Boolean> map, Boolean value) {//метод поиска ключа по значению
        for (Map.Entry<String, Boolean> entry : map.entrySet()) {
            if (entry.getValue() == value) {
                return entry.getKey();
            }
        }
        return null;
    }

}

