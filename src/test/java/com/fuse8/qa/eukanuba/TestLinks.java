package com.fuse8.qa.eukanuba;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class TestLinks extends BaseClass {

    @Test(enabled = true)
    public void testMainMenu() {
        //list of links from main menu
        List<String> menuElementList = driver.findElements(By.xpath("//ul[@data-nav-name='Header navigation']//a")).stream()
                .filter(el -> el.getAttribute("href") != null)
                .map(el -> el.getAttribute("href"))
                .distinct()
                .collect(Collectors.toList());

        for (String element : menuElementList) {
            if (element != null) { //if element's link is not empty
                String url = element.replace(urlDomain, ""); //replace beginning of the link https://www.eukanuba.com if it exists
                if ((url.startsWith("/") && (!url.startsWith("/" + urlLocation)))) {
                    Assert.fail("Alien element is found: " + element);
                }
            }
        }
    }

    @Test()
    public void testAllLinks() throws IOException { // pick up all links from site

        //Close cookie window
        WebDriverWait waiter = new WebDriverWait(driver, 5);
        //Wait<WebDriver> waiter = new FluentWait<WebDriver>(driver).withTimeout(10, TimeUnit.SECONDS).ignoring(NoSuchFrameException.class);
        try {//wait until cookie window appear
            waiter.until(ExpectedConditions.visibilityOfElementLocated(By.id("onetrust-banner-sdk")));
            //and close it
            driver.findElement(By.xpath("//div[@id = 'onetrust-banner-sdk']//button[contains(@class, 'banner-close-button onetrust-lg') or @id = 'onetrust-accept-btn-handler']")).click();
        } catch (TimeoutException e) {
            System.out.println("no cookie");
        }

        Map<String, Boolean> allLinksMap = new HashMap<>();
        allLinksMap.put(url, false);

        int errorCount = 0;
        int emptyPageCount = 0;

        while (true) {
            //Get non-visited link or break loop if all links visited (all links are visited = all values in the map are true)
            String currentLink = getKey(allLinksMap, false);
            if (currentLink == null) {
                break; // break loop condition - all links are visited (all values are true). See getKey method
            }
            System.out.println(currentLink);

            //Open non-visited link from map
            driver.get(currentLink);

            //Check if menu header links are not white
//            driver.findElement(By.xpath("//button[contains(.,'Menu')]")).click(); //first open main menu
//            List<WebElement> headerLinks = driver.findElements(By.xpath("//ul[@data-nav-name='Header navigation']//a[@class='euka-list__header']"));
//            for (WebElement l : headerLinks) {
//                if (l.getCssValue("color").equals("rgba(255, 255, 255, 1)")) {
//                    System.out.println("Attention! Header link in menu is white! " + currentLink);
//                    break;
//                }
//            }

            //checking if opened page has any content
            try {
                driver.findElement(By.xpath("//main/div | //main/section"));
            } catch (NoSuchElementException e) {
                System.out.println("Attention! Current page is empty: " + currentLink);
                emptyPageCount++;
            }

            //Mark it as visited
            allLinksMap.put(currentLink, true); //ссылка заменит саму себя, тк ключи не могут совпадать

            //Find all links on open page
            List<String> allPageLinks = driver.findElements(By.xpath("//a"
//                    "[" +
//                    "starts-with(@href, '" + urlDomain + "') or " +
//                    "starts-with(@href,'/')]"
            )).stream()
                    .filter(el -> el.getAttribute("href") != null && !el.getAttribute("href").startsWith("tel:"))
                    .map(el -> el.getAttribute("href").replaceAll("[\\/#]*$", ""))
                    .distinct()
                    .collect(Collectors.toList());

            //Add all new links that are missed in the map
            for (String pageLink : allPageLinks) {
                //Check if link exists (not 404)
                if (!allLinksMap.containsKey(pageLink)) {
                    URL testUrl = new URL(pageLink);
                    HttpURLConnection huc = (HttpURLConnection) testUrl.openConnection();
                    int responseCode = huc.getResponseCode();
                    if (responseCode == HttpURLConnection.HTTP_NOT_FOUND) {
                        System.out.println("Attention! Page " + pageLink + " is not found! Link is placed on " + currentLink);
                        errorCount++;
                    }
                    huc.disconnect();
                }

                if ((pageLink.startsWith(urlDomain) && (!pageLink.startsWith(url)))) {
                    System.out.println("Attention!!! Alien element is found: " + pageLink + " on page: " + currentLink);
                    allLinksMap.put(pageLink, true);
                    errorCount++;
                    continue;
                }
                if (!pageLink.startsWith(urlDomain)) {
                    allLinksMap.put(pageLink, true); // to avoid duplication of info about external links (e.g. facebook account)
                    continue;
                }

//              if (!allLinksMap.containsKey(pageLink) && !pageLink.contains("au")){
//                 allLinksMap.put(pageLink, false);
                allLinksMap.putIfAbsent(pageLink, false);
            }
        }

        //Final result messages
        System.out.println("Tests done. " + allLinksMap.size() + " pages were verified.");
        if (emptyPageCount > 0) {
            System.out.println("Count of empty pages: " + emptyPageCount + ". Please check them!");
        }
        if (errorCount > 0) {
            Assert.fail("Test failed! See errors above ↑. Count of errors = " + errorCount);
        }
    }

    public String getKey(Map<String, Boolean> map, Boolean value) {//метод поиска ключа по значению
        for (Map.Entry<String, Boolean> entry : map.entrySet()) {
            if (entry.getValue() == value) {
                return entry.getKey();
            }
        }
        return null;
    }
}
