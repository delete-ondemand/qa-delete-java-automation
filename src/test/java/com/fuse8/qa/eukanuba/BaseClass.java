package com.fuse8.qa.eukanuba;

import com.fuse8.qa.TestNGHelper;

public class BaseClass extends TestNGHelper {

    static {
        urlLocation = "au";
        urlDomain = "https://www.eukanuba.com";
        testName = "Eukanuba Test";
        hasNetwork = false;
        hasVisual = false;
        hasVideo = true;
        hasConsole = false;
    }
}
