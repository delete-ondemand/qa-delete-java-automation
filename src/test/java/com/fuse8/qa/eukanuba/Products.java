package com.fuse8.qa.eukanuba;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.function.Function;

public class Products extends BaseClass {

    @Test(enabled = true)
    public void findProduct() throws Exception {

        WebDriverWait waiter = new WebDriverWait(driver, 5);
        //Wait<WebDriver> waiter = new FluentWait<WebDriver>(driver).withTimeout(10, TimeUnit.SECONDS).ignoring(NoSuchFrameException.class);
        try {//wait until cookie window appear
            waiter.until(ExpectedConditions.visibilityOfElementLocated(By.id("onetrust-banner-sdk")));
            //and close it
            driver.findElement(By.xpath("//div[@id = 'onetrust-banner-sdk']//button[contains(@class, 'banner-close-button onetrust-lg') or @id = 'onetrust-accept-btn-handler']")).click();
        } catch (TimeoutException e) {
            System.out.println("no cookie");
        }

        //click to "Product" navigation link
        driver.findElement(By.xpath("//a[contains(@href, '/products')]")).click();
        // click to "View all products" link
        driver.findElement(By.xpath("//a[contains(text(),'View all products')]")).click();

        // find "Senior Small Breed Dry Dog Food" product, scroll to it and click
        WebElement element = waiter.until(new Function<WebDriver, WebElement>() {
            public WebElement apply(WebDriver driver) {
                return driver.findElement(By.xpath("//a[contains(@data-product-details, 'Senior Small Breed Dry Dog Food')]//span[contains(text(), 'View Product')]"));
            }
        });
        //WebElement element = driver.findElement(By.xpath("//a[contains(@data-product-details, 'Senior Small Breed Dry Dog Food')]//span[contains(text(), 'View Product')]"));
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(false);", element);
        Thread.sleep(500);
        element.click();

        //check if there is a button "Shop now" on the page of the product
        try {
            waiter.until(new Function<WebDriver, WebElement>() {
                public WebElement apply(WebDriver driver) {
                    return driver.findElement(By.xpath("//div[contains(@data-track-custom-category,'Buy Now')]"));
                }
            });
            //driver.findElement(By.xpath("//div[contains(@data-track-custom-category,'Buy Now')]"));
        } catch (TimeoutException e) {
            //System.out.println("Attention! Current product cannot be bought!");
            Assert.fail("Attention! Current product cannot be bought!");
        }
    }
}
