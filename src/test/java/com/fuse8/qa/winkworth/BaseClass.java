package com.fuse8.qa.winkworth;

import com.fuse8.qa.TestNGHelper;

public class BaseClass extends TestNGHelper {
    static {
        testName = "Winkworth Test";
        urlDomain = "https://winkworth-uat-mvc-13.azurewebsites.net";
        hasNetwork = false;
        hasVisual = false;
        hasVideo = true;
        hasConsole = false;
    }
}
