package com.fuse8.qa.winkworth;

import com.github.romankh3.image.comparison.ImageComparison;
import com.github.romankh3.image.comparison.ImageComparisonUtil;
import com.github.romankh3.image.comparison.model.ImageComparisonResult;
import com.github.romankh3.image.comparison.model.ImageComparisonState;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class HomePage extends BaseClass {
    static DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy.MM.dd-HH.mm.ss");
    static LocalDateTime initialTime = LocalDateTime.now();
    LocalDateTime now = LocalDateTime.now();

    private final Logger logger = LogManager.getLogger(this.getClass());

    @BeforeClass(enabled = true)
    public void closeCookieOnHomePage() throws InterruptedException {
        //close cookie popup
        driver.get(url);
        WebDriverWait waiter = new WebDriverWait(driver, Duration.ofSeconds(5L));
        try {//wait until cookie window appear
            waiter.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[contains(text(), 'Accept All Cookies')]")));
            //and close it
            driver.findElement(By.xpath("//button[contains(text(),'Accept All Cookies')]")).click();
        } catch (TimeoutException e) {
            logger.info("no cookie");
        }
        try {//wait until chat window appear
            waiter.until(ExpectedConditions.visibilityOfElementLocated(By.className("c4b-embed-parent")));
            //and close it
            driver.findElement(By.cssSelector("div>button[title='End Chat']")).click();
        } catch (TimeoutException e) {
            logger.info("no Chat", e);
        }
        Thread.sleep(1000);
    }

    @Test(enabled = true)
    public void screenShot1HomeNav() throws IOException, InterruptedException {
        componentComparison("div.site-header", "MainNav.png", "Main navigation");
    }

    @Test(enabled = true)
    public void screenShot2HomeHero() throws IOException, InterruptedException {
        componentComparison("div.office-hero__image", "HomeHero.png", "Home hero banner");
    }

    @Test(enabled = true)
    public void screenShot3HomePromo() throws IOException, InterruptedException {
        componentComparison("article.promo", "HomePromo.png", "Home promo banner");
    }


    public void componentComparison(String selector, String expectedImg, String componentName) throws IOException, InterruptedException {

        WebElement element = driver.findElement(By.cssSelector(selector));
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(false);", element);
        Thread.sleep(500);

        File f = element.getScreenshotAs(OutputType.FILE);// capture screenshot of component
        // FileUtils.copyFile(f, new File("MainNav.png"));
        BufferedImage actualImage = ImageIO.read(f); // current image

        BufferedImage expectedImage = ImageComparisonUtil.readImageFromResources("winkworth/" + expectedImg); // get reference image from resources

        //Create ImageComparison object and compare the images
        ImageComparisonResult imageComparisonResult = new ImageComparison(expectedImage, actualImage).compareImages();

        //Check the result
        if (ImageComparisonState.MATCH == imageComparisonResult.getImageComparisonState()) {
            logger.info(componentName + " wasn't changed " + dtf.format(now));
        } else {
            File resultDestination = new File("logs/result" + expectedImg);
            //Create ImageComparison object with result destination and compare the images.
            ImageComparisonResult mainNavComparison = new ImageComparison(expectedImage, actualImage, resultDestination).compareImages();
            Assert.fail(componentName + " was changed " + dtf.format(now));
        }
    }
}