package com.fuse8.qa.UlsterRugby;

import com.fuse8.qa.TestNGHelper;
import org.testng.annotations.DataProvider;

public class BaseClass extends TestNGHelper {
    //Browser authentication
    String name = "ulster-prod";
    String pass = "445OqUyfa89q";

    static {
        testName = "Ulster screenshot Test";
        urlDomain = "https://ulster-stage.incrowdsports.com";
        hasNetwork = false;
        hasVisual = false;
        hasVideo = true;
        hasConsole = false;
    }
}
