package com.fuse8.qa.UlsterRugby;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import java.time.Duration;
import java.util.List;
import java.util.stream.Collectors;

public class SEO extends BaseClass {

    @Test(enabled = true)
    public void GetUrls() {
      //  driver.get("https://" + name +":" + pass + "@prod.ulster.rugby/mainsitemap.xml");
        driver.get("https://ulster.rugby/mainsitemap.xml");

        List<String> allSEOLinks = collectLinks("span");
        System.out.println(allSEOLinks.size());

        driver.get("https://ulster-prod.incrowdsports.com");
        WebDriverWait waiter = new WebDriverWait(driver, Duration.ofSeconds(5L));
        try {//wait until cookie window appear
            waiter.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[contains(text(), 'ALL COOKIES')]")));
            //and close it
            driver.findElement(By.xpath("//button[contains(text(),'ALL COOKIES')]")).click();
        } catch (TimeoutException e) {
            System.out.println("no cookie");
        }

        for (String link : allSEOLinks) {
            driver.get(link);
            try {
                //find sponsor
                WebElement element = waiter.until(driver -> driver.findElement(By.xpath("//p[contains(@class,'text-caption mt-2')]")));
//               ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(false);", element);
//               Thread.sleep(500);
                //driver.findElement(By.xpath("//img[contains(@alt,'Kingspan')]"));
            } catch (NoSuchElementException | TimeoutException e) {
                System.out.println("Empty page " + link);
            }
        }

    }

    private List<String> collectLinks(String css) {
        return driver.findElements(By.cssSelector(css)).stream()
                .filter(el -> el.getText().startsWith("https"))
                .map(el -> el.getText())
                .distinct()
                .collect(Collectors.toList());
    }
}
